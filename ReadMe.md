# Introducción a Android
Este workshop tiene como objetivo el dar una breve introducción a los conocimientos básicos necesarios para escribir aplicaciones Android. Siguiendo los ejercicios contenidos en este workshop, el estudiante establecerá las bases para escribir aplicaciones altamente funcionales y escalables.

# Requisitos #

Para este workshop es necesario instalar:

* [Java SE Development Kit 7](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
* [Android Studio](https://developer.android.com/studio/index.html)
* [VirtualBox](https://www.virtualbox.org/)
* [Genymotion](https://www.genymotion.com/) (Requiere que generen una cuenta, sin embargo, es gratis)
* [El plugin de Genymotion para Android Studio](https://www.genymotion.com/plugins/) (Una vez instalado Android Studio)

El ejercicio de este taller requiere que se tenga instalada la version API 22 (5.1.1, Lollipop); para ello, es necesario installar los paquetes pertenecientes a esta plataforma en el administrador del sdk (SDK Manager):

![figure2.jpg](https://bitbucket.org/repo/jdx8zg/images/2708881340-figure2.jpg)

![Android_studio_sdk_settings.png](https://bitbucket.org/repo/jdx8zg/images/1816488072-Android_studio_sdk_settings.png)

También es recomendado que el estudiante descargue, en **Genymotion**, un dispositivo virtual con Android API 22 (Lollipop):

![Screenshot from 2016-07-25 23-49-01.png](https://bitbucket.org/repo/jdx8zg/images/2957107839-Screenshot%20from%202016-07-25%2023-49-01.png)


## Ejercicio 1: Vincular interfaz de Usuario de una Actividad
Editar una interfaz gráfica en xml es simple usando Android Studio; sin embargo, es importante conocer las propiedades de los distintos elementos, mismas que pueden ser manipuladas empleando xml como lenguaje de marcado.
 
Primero, se deben cambiar los identificadores de las vistas contenidas en el layout **activity__main.xml**

*nota: es necesario que los identificadores sean unicos, al menos, dentro de un mismo archivo xml.*

Primero el botón de accion:

```
 <Button
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="New Button"
        android:id="@+id/boton_accion"/>
```

Despues el *TextView* que se encuentra debajo del mismo:

```
<TextView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textAppearance="?android:attr/textAppearanceLarge"
        android:id="@+id/texto_nombre"
        android:visibility="invisible" />
```

Ahora que, está lista nuestra interfaz de usuario, procedemos a crear las variables que representarán cada uno de nuestros componentes.

En la clase **app/src/main/java/com/example/emmanuel/workshop/MainActivity.java**, se agregan las variables que designan cada uno de los componentes:

```
private Button accionBtn;
private TextView mensajeTxt;
```

Despues, se asignan las variables buscando sus respectivos componentes usando sus identificadores como referencia:

```
accionBtn = (Button) findViewById(R.id.boton_accion);
mensajeTxt = (TextView) findViewById(R.id.texto_mensaje);
```

Ahora necesitamos un método que establezca el texto del **TextView** _mensajeTxt_ como un saludo al nombre que escribamos en el **EditText** _cajaTxt_:

```
    private void saludo(){
        if(cajaTxt.getText().toString().isEmpty()){
            mensajeTxt.setVisibility(View.GONE);
        }else {
            mensajeTxt.setText("Hola " + cajaTxt.getText() + "!!!");
            mensajeTxt.setVisibility(View.VISIBLE);
        }
    }
```
Esta función será invocada al momento de precionar el botón _accionBtn_:

```
        accionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saludo();
            }
        });
```

###MOMENTO!!!###

En _Android_ es una buena práctica el establecer los textos empleando recursos en un archivo xml. Por lo tanto, hemos de agregar dos textos estáticos (para el **hint** del campo 'nombre' y el texto en el botón de saludo) y un texto dinánico (para el texto de saludo) en el archivo xml designado por convención para albergar dichos textos, **app/src/main/res/values/strings.xml**:
 
```
<string name="hint_name">name</string>
<string name="texto_saludo">Hola %1$s!!!</string>
<string name="btn_saludo">Saludar</string>
```

Los textos estáticos se implementan de manera sensilla, referenciándolos en el layout **activity__main.xml**


```
<EditText
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:inputType="textPersonName"
        android:ems="10"
        android:id="@+id/nombreTxt"
        android:hint="@string/hint_name" />
```
       
```
<Button
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/btn_saludo"
        android:id="@+id/boton_accion"/>
```
  
Así mismo, debemos implementar el texto dinamico empleando el método _format_ de la clase **String**:

```
mensajeTxt.setText(String.format(getResources().getString(R.string.texto_saludo),cajaTxt.getText()));
```

## Ejercicio 2: Abrir una nueva actividad, enviar y recibir información de ella
Una de las operaciones fundamentales que debe realizar un programador Android es abrir nuevas actividades que permitan al usuario interactuar con algun nuevo módulo de su aplicacion. Para comenzar con esta tarea, primero es necesario crear una nueva actividad llamada "PetChoiceActivity.java":

```
package com.example.emmanuel.workshop;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PetChoiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_choice);

    }
}
```

Esta nueva actividad deberá tener tambien un archivo que defina su layout ("activity_pet_choice.xml"):

Primero se añaden lo elementos que conformarán la vista:

    -Un Spinner
    -Un ImageView
    -Un Button
    
Ahora, hemos de envolver el LinearLayout (que sirve de base) en un ScrollView, de modo que nuestra vista sea visible, aun cuando abarque más de la altura de la pantalla:

```
<?xml version="1.0" encoding="utf-8"?>
<ScrollView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_height="match_parent"
    android:layout_width="match_parent">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:paddingBottom="@dimen/activity_vertical_margin"
        android:paddingLeft="@dimen/activity_horizontal_margin"
        android:paddingRight="@dimen/activity_horizontal_margin"
        android:paddingTop="@dimen/activity_vertical_margin"
        tools:context="com.example.emmanuel.workshop.PetChoiceActivity"
        android:orientation="vertical"
        android:gravity="center_horizontal">

        <Spinner
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/spinner"
            android:layout_margin="10dp"
            android:entries="@array/arreglo_mascotas" />

        <ImageView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/imagen"
            android:src="@drawable/default_image"
            android:scaleType="centerCrop" />

        <Button
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/btn_volver"
            android:id="@+id/boton_finalizar"
            android:layout_marginTop="30dp" />

    </LinearLayout>
</ScrollView>
```

Ahora debemos poblar el **Spinner**. Dado que el contenido de este Spinner es estático, la mejor forma de poblarlo es empleando un arreglo de cadenas definido en un archivo xml.

primero, definiremos algunas cadenas nuevas en nuestro _archivo strings.xml_:

```    
    <string name="titulo_dinamico">Elige mascota para %1$s</string>
    
    <string name="ninguno">Ninguno</string>
    <string name="gatito">Gatito</string>
    <string name="perrito">Perrito</string>

    <string name="btn_elegir_mascota">Elegir marcota...</string>
    <string name="btn_volver">Volver</string>
```

Después, procedemos a enlistar el contenido del arreglo de opciones empleando los textos "ninguno", "gatito" y "perrito", en un archivo llamado **/main/res/values/arrays.xml**:

```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string-array name="arreglo_mascotas">
        <item>@string/ninguno</item>
        <item>@string/gatito</item>
        <item>@string/perrito</item>
    </string-array>
</resources>
```

El arreglo de cadenas de texto puede establecerse empleando la propiedad **entities** de la clase _Spinner_:

```
<Spinner
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:id="@+id/spinner"
        android:layout_below="@+id/titulo"
        android:layout_centerHorizontal="true"
        android:layout_margin="10dp"
        android:entries="@array/arreglo_mascotas" />
```

Hay que crear una clase Java que se convierta en nuestra actividad de selección de mascota. 

Se llamará: **app/src/main/java/com/example/emmanuel/workshop/PetChoiceActivity.java**


```
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PetChoiceActivity extends AppCompatActivity {


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PetChoiceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_choice);

    }
}
```

Para invocar esta nueva actividad es necesario crear un nuevo botón y una nueva etiqueta (la cual reflejará el resultado de la actividad) en el layout **activity__main.xml**

```
    <Button
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/btn_elegir_mascota"
        android:id="@+id/boton_elegir_mascota"
        android:layout_marginTop="30dp" />

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:textAppearance="?android:attr/textAppearanceLarge"
        android:id="@+id/texto_mascota"
        android:visibility="gone" />
```

```
private Button elegirMascotaBtn;
private TextView mensajeMascotaTxt;
.
.
.
elegirMascotaBtn = (Button) findViewById(R.id.boton_elegir_mascota);
mensajeMascotaTxt = (TextView) findViewById(R.id.texto_mascota);
```

Antes de continuar, debemos declarar un par de variables estaticas en la clase **PetChoiceActivity.java** (mismas que nos ayudarán a controlar mejor el flujo de información entre ambas actividades:

```    
    static final String NOMBRE = "nombre";
    static final int PET_CHOICE_ACTIVITY = 1234; //de preferencia, este número debe ser único
```    

Ahora procedemos a crear el método **elegirMascota()** (el cual abre la actividad de seleccion de mascota):

```
    private void elegirMascota(){
        Intent intent = new Intent(this, PetChoiceActivity.class);
        intent.putExtra(PetChoiceActivity.NOMBRE, cajaTxt.getText().toString());
        
        startActivityForResult(intent,  PetChoiceActivity.PET_CHOICE_ACTIVITY);
    }
```

Y lo asignamos al boton "**elegirMascotaBtn**":

```        
        elegirMascotaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                elegirMascota();
            }
        });
```

En **PetChoiceActivity.java** enlazamos los distintos componentes que conforman la interfaz de usuario:

```
    private Spinner spinner;
    private ImageView imagnMascota;
    private Button volverBtn;
    .
    .
    .
   spinner = (Spinner) findViewById(R.id.spinner);
   imagnMascota = (ImageView) findViewById(R.id.imagen);
   volverBtn = (Button) findViewById(R.id.boton_finalizar);
``` 

A continuación, vamos a mostrar un titulo que refleje la cadena (el nombre) que ha sido transmitida por la actividad anterior; Primero, tomamos el texto añadido por la actividad anterior y lo establecemos en la barra de acción de la actividad **PetChoiceActivity.java** (esto se hace en _onCreate()_):

```
    String nombre = this.getIntent().getExtras().getString(NOMBRE);
    setTitle(String.format(getResources().getString(R.string.titulo_dinamico),nombre));
```

Debemos definir un _Listener_ que cambie la imagen del _ImageView_ de acuerdo a la selección del _Spinner_:

```        
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = getResources().getStringArray(R.array.arreglo_mascotas)[position];

                if(selection.contentEquals(getResources().getString(R.string.ninguno))){
                    imagnMascota.setImageResource(R.drawable.default_image);
                }else if(selection.contentEquals(getResources().getString(R.string.gatito))){
                    imagnMascota.setImageResource(R.drawable.kitty);
                }else if(selection.contentEquals(getResources().getString(R.string.perrito))){
                    imagnMascota.setImageResource(R.drawable.puppy);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //No hacer nada
            }
        });
```

Por último, es necesario definir una función que devuelva la mascota elegida como resultado. Para esto, es necesario definir otra variable estatica y final:

```    
static final String MASCOTA = "mascota";
```

Nuestra función luce así:

```
    private void regresarResultado(){
        Intent resultado = new Intent();
        resultado.putExtra(MASCOTA
                ,getResources().getStringArray(R.array.arreglo_mascotas)[spinner.getSelectedItemPosition()]);
        
        setResult(Activity.RESULT_OK, resultado);
    }
```    

Esta función será implementada por el botón "volver":

```
        volverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresarResultado();
                finish();
            }
        });
```

También, es necesario implementar esta función en el botón de retroceso (para evitar resultados inesperados al cerrar la actividad por medio de hardware):

```
 @Override
    public void onBackPressed() {
        regresarResultado();
        //La función "regresarResultado()" debe escribirse antes de retomar la función original
        super.onBackPressed();
    }
```

Es momento de recibir el resultado de la selección de mascota, para ello se añadirán dos nuevos textos a nuestro archivo **string.xml**:

```
    <string name="resultado_dinamico">Mascota elegida: %1$s</string>
    <string name="resultado_ninguno">Ninguna mascota fué elegida</string>
```

Por último, se sobreescribe la función onActivityResult() para recibir el resultado de la actividad de selección de mascota:

```
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PetChoiceActivity.PET_CHOICE_ACTIVITY){
            if(resultCode == RESULT_OK){
                String result = data.getExtras().getString(PetChoiceActivity.MASCOTA);

                if(result.contentEquals(getString(R.string.ninguno))){
                    mensajeMascotaTxt.setText(getString(R.string.resultado_ninguno));
                }else if(result.contentEquals(getString(R.string.gatito))
                 || result.contentEquals(getString(R.string.perrito))){
                    mensajeMascotaTxt.setText(String.format(getString(R.string.resultado_dinamico),result));
                }

                mensajeMascotaTxt.setVisibility(View.VISIBLE);
            }
        }
    }
```


## Ejercicio 3: Hacer que la aplicación sea multilenguaje

Primero creamos una carpeta en la localización _app/src/main/res/_ llamada **values-es**. 

Luego copiamos en ella nuestro archivo **strings.xml**. 

Por último, traduciremos las cadenas de nuestro archivo **app/src/main/res/values/strings.xml** a inglés (el primero que teníamos, ya que el idioma por defecto de Android es el inglés)

```
<resources>
    <string name="app_name">Workshop Intro Android</string>
    <string name="hint_name">name</string>
    <string name="texto_saludo">Hey, %1$s!!!</string>
    <string name="btn_saludo">Greetings</string>

    <string name="titulo_dinamico">Choose a pet for %1$s</string>
    <string name="ninguno">None</string>
    <string name="gatito">Kitty</string>
    <string name="perrito">Puppy</string>

    <string name="btn_elegir_mascota">Choose a pet...</string>
    <string name="btn_volver">Return</string>

    <string name="resultado_dinamico">Chosen pet: %1$s</string>
    <string name="resultado_ninguno">No pet was chosen</string>

</resources>
```

NOTA: Esta forma de traducir los textos es poco práctica (fué impartida para ilustrar el uso de carpetas en Android). Android Studio cuenta con un editor de cadenas muy práctico como se ve a continuación:

1. Primero se abre el archivo por defecto (es decir el archivo de cadenas localizado en **app/src/main/res/values/strings.xml**). En el borde superior se puede observar un enlace que ofrece abrir el editor de cadenas de Android:

![Screenshot from 2016-07-25 22-52-18.png](https://bitbucket.org/repo/jdx8zg/images/3075813008-Screenshot%20from%202016-07-25%2022-52-18.png)

El editor luce de esta manera: 

![Screenshot from 2016-07-25 22-53-00.png](https://bitbucket.org/repo/jdx8zg/images/3003718263-Screenshot%20from%202016-07-25%2022-53-00.png)

2. Procedemos a hacer clic en el ícono con forma de globo terráqueo (se encuentra junto al signo de adición  color verde en la esquina superior izquierda del editor); éste abrirá un selector de idiomas que creará los campos correspondientes a la traducción de cada cadena de texto:

![Screenshot from 2016-07-25 23-04-48.png](https://bitbucket.org/repo/jdx8zg/images/2184842009-Screenshot%20from%202016-07-25%2023-04-48.png)

Evidentemente, se ha creado una nueva carpeta con la dirección **app/src/main/res/values-it** (y su respectivo archivo **string.xml**):

![Screenshot from 2016-07-25 23-05-30.png](https://bitbucket.org/repo/jdx8zg/images/2466769194-Screenshot%20from%202016-07-25%2023-05-30.png)

El contenido del archivo **app/src/main/res/values-it/strings.xml** del ejercicio resuelto luce asi:

```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="app_name">Workshop Intro Android</string>
    <string name="btn_elegir_mascota">Scheliere un animale domestico...</string>
    <string name="btn_saludo">Salutare</string>
    <string name="gatito">Gattino</string>
    <string name="btn_volver">Ritornare</string>
    <string name="perrito">Cucciolo </string>
    <string name="hint_name">nome</string>
    <string name="ninguno">Nessuno</string>
    <string name="resultado_dinamico">Animale scelto: %1$s</string>
    <string name="resultado_ninguno">Nessun degli animali è stato scelto</string>
    <string name="texto_saludo">Ciao, %1$s!!!</string>
    <string name="titulo_dinamico">Sceliere un animale per  %1$s</string>
</resources>
```